(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['spritensheet'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('spritensheet'));
    } else {
        // Browser globals (root is window)
        root.Spriten = factory(root.SpritenSheet);
    }
}(this, function (SpritenSheet) {

  var Spriten = function(spritensheet) {
    this.sheet = spritensheet;
    this.sequence;
    this.frame;
    this.fps;
  };

  Spriten.prototype = {
    // Set the animation sequence to use
    use: function(name) {

    },

    // Advance the current animation sequence by one frame
    advance: function() {

    },

    image: function() {
      return this.sheet.image;
    },

    blit: function(surface) {

    }
  };

  return Spriten;

}));
