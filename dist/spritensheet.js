(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.SpritenSheet = factory();
    }
}(this, function () {

/*
{
  sheet: {
    image: '',
    width: 200,
    height: 200
  },
  frame: {
    width: 50;
    height: 80
  },
  animations: [
    { name: 'stand',
      frames: 3,
      fps: 6
    },

    { name: 'walk',
      frames: 6,
      fps: 6
    },

    { name: 'run',
      frames: 6,
      fps: 6
    }
  ]
}
*/

  var SpritenSheet = function(def) {
    this.def        = def;
    this.image      = new Image(def.sheet.image);
    this.animations = {};

    var x = 0;
    var y = 0;
    for (y=0, len=def.animations.length; y<len; y++) {
      for (x=0, len=sequence.frames; x<len; x++) {
        this.animation[def.animations[y].name] = [];
        x = x * def.frame.width;
        y = y * def.frame.height;
      }
    }
  };

  SpritenSheet.prototype = {

  };

  return SpritenSheet;

}));

